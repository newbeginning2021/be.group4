const mongoose = require("mongoose");

const SchoolSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },

    address: {
      type: String,
      required: true,
    },

    id: {
      type: String,
    },
  },
  { timestamps: true }
);

const School = mongoose.model("School", SchoolSchema);
module.exports = School;
