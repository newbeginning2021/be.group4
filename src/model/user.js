const mongosse = require("mongoose");
const bcrypt= require("bcryptjs")
const userSchema = new mongosse.Schema({
    id: {
        type: String,
    },

    name: {
        type: String,
    },

    email: {
        type: String,
        unique: true,
        required: true,
        trim: true,
    },

    school_id: {
        type: String,
    },

    account: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
});
userSchema.pre('save', async function (next) {
    const user = this;
    
    if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
    }
    next();
    });

userSchema.statics.findByCredentials = async (inputData, password) => {
    const user = await User.findOne({email:inputData}) || await User.findOne({name:inputData});
    if (!user) {
    return user
    }
    
    const isMatch = await bcrypt.compare(password, user.password);
    
    if (!isMatch) {
    return false
    }
    return user;
    };

const User = mongosse.model("User", userSchema);
module.exports = User;
