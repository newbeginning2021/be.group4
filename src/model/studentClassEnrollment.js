const mongoose = require("mongoose");

const StudentEnrollmentSchema = new mongoose.Schema(
  {
    id: {
      type: String,
    },
    course_id: {
      type: String
    },

    student_id: {
      type: String,
    },
  },
  { timestamps: true }
);

const StudentEnrollment = mongoose.model(
  "StudentEnrollment",
  StudentEnrollmentSchema
);
module.exports = StudentEnrollment;
