const mongoose = require("mongoose");

const LessonSchema = new mongoose.Schema(
  {
    id: {
      type: String,
    },
    date: {
      type: String,
      required: true,
    },

    start_time: {
      type: String,
      required: true,
    },

    end_time: {
      type: String,
      required: true,
    },

    classId: {
      type: String
    },
    teacherId: {
      type: String
    },
    classRoomId: {
      type: String,
      required:true,
    },

  },
  { timestamps: true }
);

const Lesson = mongoose.model("Lesson", LessonSchema);
module.exports = Lesson;
