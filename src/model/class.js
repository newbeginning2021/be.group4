const mongoose = require("mongoose");

const ClassSchema = new mongoose.Schema(
  {
    id: {
      type: String,
    },

    name: {
      type: String,
    },
    start_date: {
      type: Date,
      trim: true,
      required: true,
    },
    end_date: {
      type: Date,
      trim: true,
      required: true,
    },
    school_id: {
      type: String
    },
    day: {
      type: Array,
      required: true,
    },
    teacher_id: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    classRoom_id: {
      type: String,
      required:true,
    },
  },
  { timestamps: true }
);

const Class = mongoose.model("Class", ClassSchema);
module.exports = Class;
