const mongoose = require("mongoose");

const ClassRoomSchema = new mongoose.Schema(
  {
    id: {
      type:String,
    },

    location: {
      type: String,
    },
    name: {
      type: String,
      required: true,
    },
    school_id: {
      type: String,
    },
  },
  { timestamps: true }
);

const ClassRoom = mongoose.model("ClassRoom", ClassRoomSchema);
module.exports = ClassRoom;
