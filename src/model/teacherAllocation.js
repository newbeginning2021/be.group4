const mongoose = require("mongoose");

const TeacherAllocationSchema = new mongoose.Schema({
  id: {
    type: String
  },
  teacher_id: {
    type: String
  },
  classId: {
    type: String
  },
},
{ timestamps: true }
);

const TeacherAllocation = mongoose.model(
  "TeachAllocation",
  TeacherAllocationSchema
);
module.exports = TeacherAllocation;
