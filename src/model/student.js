const mongoose = require("mongoose");

const studentSchema = new mongoose.Schema(
  {
    id: {
      type: String,
    },
    name: {
      type: String,
    },
    sex: {
      type: String,
    },
    Dob: {
      type: String,
    },
    phone: {
      type: String,
    },
    email: {
      type: String,
      required: true,
      trim: true,
      index: true,
    },
    address: {
      type: String,
    },
    school_id: {
      type: String,
    },
    img: {
        type:String
      }
    },
  { timestamps: true }
);

const Student = mongoose.model("Student", studentSchema);
module.exports = Student;
