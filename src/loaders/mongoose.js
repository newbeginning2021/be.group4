const mongoose = require('mongoose');

module.exports = async function () {
    const connection = await mongoose.connect('mongodb+srv://admin:gr@88538021@cluster0.r5jbq.mongodb.net/ivyschoolmanagementsystem?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    });
    return connection.connection.db;
};