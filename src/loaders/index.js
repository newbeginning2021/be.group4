const koaRouterLoader = require('./koa');
const mongooseLoader = require('./mongoose');

exports.init = (app) => {
    mongooseLoader();
    koaRouterLoader(app);

};