const apiRouter = require("../../src/routes/v1/api");



module.exports = async (app) => {
    app.use(apiRouter.routes());
    return app;
};
