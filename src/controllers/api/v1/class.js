const Course = require("../../../model/class");
const User = require("../../../model/user");
const StudentEnrollment = require("../../../model/studentClassEnrollment");
const Teacher = require('../../../model/teacher');
const Classroom = require('../../../model/classRoom');
const Lesson = require("../../../model/lesson");
const TeacherAllocation = require("../../../model/teacherAllocation");

exports.index = async (ctx) => {
  const findResult = await Course.find();
  findResult.map((item) => {

  });

  ctx.body = findResult;
};

exports.store = async (ctx) => {
  const { body } = ctx.request;
  const latest = await Course.find().sort({ _id: -1 }).limit(1)
  let newId = ''
  if (latest.length !== 0) {
    const latestId = latest[0].id
    newId = `${latestId*1+1}`
  } else { 
    newId='1'
  }
  const newData = {
    ...body,
    id:newId
  }
  const course = new Course(newData);
  try {
    const courseInfo = await course.save();
    ctx.status = 201;
    ctx.body = courseInfo;
  } catch (e) {
      ctx.body = e;
  }
};

exports.delete = async (ctx) => {
  const { id } = ctx.params;
  const { n } = await Course.deleteOne({ id: id });
  const { b } =await Lesson.deleteMany({classId:id});
  const { c } =await TeacherAllocation.deleteOne({classId:id});
  const { d } =await StudentEnrollment.deleteMany({course_id:id});
  if (n === 0) {
    ctx.body = {
      message: `${id} not found!`,
    };
    ctx.status = 404;
  } else {
    ctx.body = {
      message: `${id} deleted!`,
    };
  }
};

exports.show = async (ctx) => {
  const { id } = ctx.params;
  const searchResult = await Course.find({ id: id });
  ctx.body = searchResult;
};

exports.update = async (ctx) => {
  const { id } = ctx.params;
  const updateResult = await Course.findOneAndUpdate(
    { _id: id },
    { name: "react" },
    { new: true }
  );
  ctx.body = updateResult;
};

exports.classList = async (ctx) => {
    let { pageSize, page } = ctx.request.query;
    pageSize = parseInt(pageSize);
    page = parseInt(page);
    const skip = pageSize * (page - 1);
    const res = await Course.find().skip(skip).limit(pageSize);
    if(!res) {
        ctx.body = {
            message: "404 not found"
        }
        ctx.status = 404;
    } else {
        ctx.body = res;
    }
}
exports.getClassesByTeacher = async (ctx) => {
  const {id} = ctx.params;
  try{
    const res = await Course.aggregate([
      {
        $match: {
          teacher_id:id
        }
      },
      {
        $lookup: {
          from: 'teachers',
          localField: 'teacher_id',
          foreignField: 'id',
          as: 'teachers',
        }
      },
      {
        $lookup: {
          from: 'classrooms',
          localField: 'classRoom_id',
          foreignField: 'id',
          as: 'classrooms',
        }
      },
      {
        $project: {
          _id:1,
          id:1,
          name:1,
          start_date:1,
          end_date:1,
          teachers: {
            id:1,
            name:1,
          },
          classrooms: {
            id:1,
            name:1,
          }
        }
      }
    ])
    if (!res) {
      ctx.body = {
        message: "404 not found"
      }
      ctx.status = 404;
    } else {
      ctx.body = res;
    }
  }catch(e){
    ctx.body = e;
  }
}
exports.getClassesByClassroom = async (ctx) => {
  const {id} = ctx.params;
  try{
    const res = await Course.aggregate([
      {
        $match: {
          classRoom_id:id
        }
      },
      {
        $lookup: {
          from: 'teachers',
          localField: 'teacher_id',
          foreignField: 'id',
          as: 'teachers',
        }
      },
      {
        $lookup: {
          from: 'classrooms',
          localField: 'classRoom_id',
          foreignField: 'id',
          as: 'classrooms',
        }
      },
      {
        $project: {
          _id:1,
          id:1,
          name:1,
          start_date:1,
          end_date:1,
          teachers: {
            id:1,
            name:1,
          },
          classrooms: {
            id:1,
            name:1,
          }
        }
      }
    ])
    if (!res) {
      ctx.body = {
        message: "404 not found"
      }
      ctx.status = 404;
    } else {
      ctx.body = res;
    }
  }catch(e){
    ctx.body = e;
  }
}
exports.getClassesByKeyword = async (ctx) => {
  const {keyword} = ctx.params;
  try{
    let res = await Course.aggregate([
      {
        $match: {
          $or: [{"id":keyword},{"name":keyword}]
        }
      },
      {
        $lookup: {
          from: 'teachers',
          localField: 'teacher_id',
          foreignField: 'id',
          as: 'teachers'
        }
      },
      {
        $lookup: {
          from: 'classrooms',
          localField: 'classRoom_id',
          foreignField: 'id',
          as: 'classrooms'
        }
      },
      {
        $project: {
          _id:0,
          id:1,
          name:1,
          start_date:1,
          end_date:1,
          teachers:{
            id:1,
            name:1,
          },
          classrooms:{
            id:1,
            name:1,
          },
        }
      }, 
    ])
    ctx.body = res;
  }catch(e){
    ctx.body = e;
  }
}
exports.getTeachers = async (ctx) => {
  try {
    const teacherInfo = await Course.aggregate([
      {
        $lookup: 
        {
          from: 'teachers',
          localField: 'teacher_id',
          foreignField: 'id',
          as: 'teachers'
        }
      },
      {
        $project: {
          _id:0,
          teachers:{
            id:1,
            name:1,
            sex:1,
            Dob:1,
            phone:1,
            email:1,
            address:1
          }
        }
      },
      {
        $unwind: "$teachers"
      },
      {
        $group: {
          _id: "$teachers"
        }
      }
    ])
    if (!teacherInfo.length) {
      ctx.body = {
        message: `no match teacher data found!`
      }
    } else {
      ctx.body = teacherInfo;
    }
  } catch (e) {
    ctx.body = e;
  }
};


exports.getClassrooms = async (ctx) => {
  try {
    const classroomsInfo = await Course.aggregate([
      {
        $lookup: 
        {
          from: 'classrooms',
          localField: 'classRoom_id',
          foreignField: 'id',
          as: 'classrooms'
        }
      },
      {
        $project: {
          _id:0,
          classrooms:{
            id:1,
            name:1,
          }
        }
      },
      {
        $unwind: "$classrooms"
      },
      {
        $group:{
          _id:'$classrooms'
        }
      }
    ])
    if (!classroomsInfo.length) {
      ctx.body = {
        message: `no match classroom data found!`
      }
    } else {
      ctx.body = classroomsInfo;
    }
  } catch (e) {
    ctx.body = e;
  }
};

exports.unenrolDetail = async (ctx) => {
  let result = ctx.classunEnrols  
  ctx.body = result;
}

exports.getStudents = async (ctx) => {
  const { id } = ctx.params;
  try {
    const studentsInfo = await StudentEnrollment.aggregate([
      {
        $match: { course_id: id }
      },
      {
        $lookup: 
        {
          from: 'students',
          localField: 'student_id',
          foreignField: 'id',
          as: 'students'
        }
      },
      {
        $unwind:"$students"
      },
      {
        $project: {
          _id: 0,
          id:1,
          students:{
            id:1,
            name:1,
            email:1,
            address:1,
            sex:1,
            Dob:1,
            school_id:1,
            phone:1,
            img:1,
            classId:1
          }
        }
      },
    ])
    if (!studentsInfo.length) {
      ctx.body = []
      // ctx.status = 404;
    } else {
      ctx.body = studentsInfo;
    }
  } catch (e) {
    ctx.body = e;
  }
};

exports.getClassDetail = async (ctx) => {
  const { classId } = ctx.params;

  let check = await Course.aggregate([
    {
      $match: {'id' : classId}
    },
    {
      $lookup: {
        from: 'teachers',
        localField: 'teacher_id',
        foreignField: 'id',
        as: 'teacher_details'
      }
    },
    {
      $lookup: {
        from: 'classrooms',
        localField: 'classRoom_id',
        foreignField: 'id',
        as: 'classroom_details'
      }
    },
    {
      $lookup: {
        from: 'lessons',
        localField: 'id',
        foreignField: 'classId',
        as: 'lesson_details'
      }
    }
   
  ])
  ctx.body = check
}
exports.enrolStudent = async (ctx) => {
  const studentEnrol = new StudentEnrollment(ctx.request.body);
  let result = await studentEnrol.save() 
  if (result) {
      ctx.body = result;
      ctx.status = 200;
  }
  else { 
      ctx.status = 500
  }
}
exports.getClassList = async (ctx) => {
  let courseInfo = await Course.aggregate([
    {
      $lookup: {
        from: 'teachers',
        localField: 'teacher_id',
        foreignField: 'id',
        as: 'teachers'
      }
    },
    {
      $lookup: {
        from: 'classrooms',
        localField: 'classRoom_id',
        foreignField: 'id',
        as: 'classrooms'
      }
    },
    {
      $project: {
        _id:0,
        id:1,
        name:1,
        start_date:1,
        end_date:1,
        teachers:{
          id:1,
          name:1,
        },
        classrooms:{
          id:1,
          name:1,
        },
      }
    }, 
  ])
  ctx.body = courseInfo
}
exports.classSelect = async (ctx) => {
  const {teacher_id, classRoom_id} = ctx.request.query;
  const classSelectInfo = await Course.aggregate([
    {
      $match: {
        $and: [
          {teacher_id},
          {classRoom_id},
        ]
      }
    },
    {
      $lookup: {
        from: 'teachers',
        localField: 'teacher_id',
        foreignField: 'id',
        as: 'teachers',
      }
    },
    {
      $lookup: {
        from: 'classrooms',
        localField: 'classRoom_id',
        foreignField: 'id',
        as: 'classrooms',
      }
    },
    {
      $project: {
        _id:1,
        id:1,
        name:1,
        start_date:1,
        end_date:1,
        teachers: {
          id:1,
          name:1,
        },
        classrooms: {
          id:1,
          name:1,
        }
      }
    }
  ])
  ctx.body = classSelectInfo;
}