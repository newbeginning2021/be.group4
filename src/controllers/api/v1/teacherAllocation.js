const TeacherAllocation = require('../../../model/teacherAllocation')

exports.getOneTeacherClass = async (ctx) => {
    console.log(ctx.params);
    const {id} = ctx.params;
    const check = await TeacherAllocation.aggregate([{
      $match: {
        'teacher_id': id
      }
    }, {
      $lookup: {
        from: 'classes',
        localField: 'classId',
        foreignField: 'id',
        as: 'class_details'
      }
    }, {
      $project: {
        'teacher_id': 1,
        'class_details': {
        'name': 1,
        'id': 1
        }
      }
    }, {
      $unwind: '$class_details'
    }, {
      $group: {
        _id: '$teacher_id',
        enrol_details: {
          $push: "$class_details"
        }
      }
    },])
    ctx.body = check;
  }

exports.addTeacherClass = async (ctx) => {
  const { body } = ctx.request;
  const latest = await TeacherAllocation.find().sort({ _id: -1 }).limit(1)
  let newId = ''
  if (latest.length !== 0) {
    const latestId = latest[0].id
    newId = `${latestId*1+1}`
  } else { 
    newId="1"
  }
  const newData = {
    ...body,
    id:newId
  }
  
  const newAllocation = new TeacherAllocation(newData);
  try {
    const result =  await newAllocation.save();
    ctx.status = 201
    ctx.body= result
  } catch (e) {
      ctx.body = e;
  }
};