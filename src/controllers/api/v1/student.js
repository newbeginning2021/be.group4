const Student = require("../../../model/student");
const StudentEnrol = require("../../../model/studentClassEnrollment"); 

exports.index = async (ctx) => {
    ctx.body = await Student.find({}, function (err, doc) {
        if (err) {
            console.log(err);
            return;
        };
    });
};

exports.store = async (ctx) => {
  const { body } = ctx.request
  const latest = await Student.find().sort({ _id: -1 }).limit(1)
  let newId = ''
  if (latest.length !== 0) {
    const latestId = latest[0].id
    newId = `${latestId*1+1}`
  } else { 
    newId='1'
  }
  const newData = {
    ...body,
    id:newId
  }
  const student = new Student(newData);
  try {
    const result =  await student.save();
    ctx.status = 201
    ctx.body= result
  } catch (e) {
      ctx.body = e;
  }


    ctx.body = 'show all students';
}


exports.delete = async (ctx) => {
    const { id } = ctx.params;
    const { n } = await Student.deleteOne({id: id});
    if ( n ===0 ) {
        ctx.status = 404
        ctx.body = {
            message: `${id} not found`
        }
    } else {
        ctx.body = {
            message: `${id} deleted`
        }
    }
}

exports.batchDelete = async (ctx) => {
    const { ids } = await ctx.request.body;
    const { n } = await Student.deleteMany({id: {$in: ids}})
    if ( n===0 ) {
        ctx.body = {
            message: `${ids} not found`
        }
    } else {
        ctx.body = {
            message: `${ids} deleted`
        }
    }
    
}

exports.show = async (ctx) => {
    const {id} = ctx.params;
    let result = await Student.find({ "id": id}, (err,result) => {
        if(err) {
            console.log(err);
        } else {
           console.log(result); 
        }
    })

    ctx.body = result;
}

exports.update = async (ctx) => {
    ctx.body = "update one student";
}


exports.studentList = async (ctx) => { 
    let { pageSize, page } = ctx.request.query;
    pageSize = parseInt(pageSize);
    page = parseInt(page);
    const skip = pageSize * (page - 1);
    const res = await Student.find().skip(skip).limit(pageSize);
    if(!res) {
        ctx.body = {
            message: "404 not found"
        }
        ctx.status = 404;
    } else {
        ctx.body = res;
    }
}

exports.findByAttribute = async (ctx) => {
    const {
        keyword
    } = await ctx.request.params;
    console.log(keyword)
    const res = await Student.find({
        $or: [{
                "name": keyword
            },
            {
                "email": keyword
            },
            {
                "phone": keyword
            }
        ]
    })
    if (!res) {
        ctx.body = {
            message: 'not found',
        }
    } else {
        ctx.body = res
    }
}
