const School = require("../../../model/school");

exports.index = async (ctx) => {
  const result = await School.aggregate([
    {
      $project: {_id:0}
    }
  ])
  ctx.body = result

}

exports.store = async (ctx) => {
  const school = ctx.request.body
  const currentId = await School.find()
  const currentSum = currentId.length;
  school.id = `${currentSum + 1}`
  const newSchool = new School(school);
  try {
    const result = await newSchool.save()
    if (result) { 
      ctx.body = result;
      ctx.status = 200
    }
  } catch (e) {
    ctx.body = {
      message: e.message,
    }
    ctx.status = 500
  }
}
exports.delete = async (ctx) => {
    ctx.body = "delete one school";
}
exports.show = async (ctx) => {
    ctx.body = "show one school";
}
exports.update = async (ctx) => {
    ctx.body = "update one school";
}

exports.showRelatedSchools = async (ctx) => {
  const { name } = ctx.params
  const regexp = new RegExp(name, 'i')
  const result = await School.find({
    name: {
    $regex:regexp
    }
  })
  ctx.body = result
}
