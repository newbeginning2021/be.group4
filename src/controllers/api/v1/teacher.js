const mongoose = require("../../../loaders/mongoose");
const Teacher = require("../../../model/teacher");
const fs = require('fs');
const AWS = require('aws-sdk');
const {params} = require('../../../config/app');
console.log('abc',params);
const s3 = new AWS.S3(params);
const url = require('url');

exports.index = async (ctx) => {
  const myUrl = new URL(ctx.url, "http:localhost:3000") 
  const school_id = myUrl.searchParams.get('school_id')
  const allTeacher = await Teacher.find({ school_id })
  if (allTeacher) {
    ctx.body = allTeacher
    ctx.status = 200
  } else { 
    ctx.status = 500
  }
}

exports.store = async (ctx) => {
  const teacher = ctx.request.body;

  const latest = await Teacher.find().sort({ _id: -1 }).limit(1)
  let newId = ''
  if (latest.length !== 0) {
    const latestId = latest[0].id
    newId = `${latestId*1+1}`
  } else { 
    newId='1'
  }
  const newData = {
    ...teacher,
    id:newId
  }

  const newTeacher = new Teacher(newData);
  try {
    const result = await newTeacher.save();
    ctx.status = 201
    ctx.body=result
  } catch (e) {
      ctx.body = e;
  }
}

exports.delete = async (ctx) => {
    const {
        id
    } = ctx.params;
    const {
        n
    } = await Teacher.deleteOne({
        id: id
    });
    if (n === 0) {
        ctx.body = {
            message: `${id} not found`
        }
    } else {
        ctx.body = {
            message: `${id} deleted`
        }
    }
}

exports.batchDelete = async (ctx) => {
    const {
        ids
    } = await ctx.request.body;
    const {
        n
    } = await Teacher.deleteMany({
        id: {
            $in: ids
        }
    })
    if (n === 0) {
        ctx.body = {
            message: `${ids} not found`
        }
    } else {
        ctx.body = {
            message: `${ids} deleted`
        }
    }
}

exports.findByAttribute = async (ctx) => {
    const { keyword } = await ctx.request.params;
   
    const res = await Teacher.find({
        $or: [{
                "name": keyword
            },
            {
                "email": keyword
            },
            {
                "phone": keyword
            }
        ]
    })
    if (!res) {
        ctx.body = {
            message: 'not found',
        }
    } else {
        ctx.body = res
    }
}

exports.show = async (ctx) => {
  const { id } = ctx.params;
  let result = await Teacher.find({"id": id}, (err,result) => {
    if(err) {
        console.log(err);
    } else {
       console.log(result); 
    }
    })
    ctx.body = result;
}

exports.update = async (ctx) => {
    ctx.body = "update one teacher";
}

exports.getTeachers = async (ctx) => {
  const res = await Teacher.find()
  if (res) {
    ctx.body = res
    ctx.status = 200
  } else { 
    ctx.status = 404
  }
}

exports.checkClass = async (ctx) => {
  const {
    id
  } = ctx.params;
  const check = await TeacherAllocation.aggregate([{
    $match: {
      'teacher_id': id
    }
  }, {
    $lookup: {
      from: 'classes',
      localField: 'course_id',
      foreignField: 'id',
      as: 'class_details'
    }
  }, {
    $project: {
      'teacher_id': 1,
      'class_details': {
        'name': 1,
        'id': 1
      }
    }
  }, {
    $unwind: '$class_details'
  }, {
    $group: {
      _id: '$teacher_id',
      enrol_details: {
        $push: "$class_details"
      }
    }
  },])
  ctx.body = check;
}
  exports.teacherList = async (ctx) => {
    let {
      pageSize,
      page
    } = ctx.request.query;
    pageSize = parseInt(pageSize);
    page = parseInt(page);
    console.log(pageSize, page)
    const skip = pageSize * (page - 1);
    const res = await Teacher.find().skip(skip).limit(pageSize);
    if (!res) {
      ctx.body = {
        message: "404 not found"
      }
      ctx.status = 404;
    } else {
      ctx.body = res;
    }
  }


exports.postData = async (ctx) => {
    const {files: {body: {path}}, body: {id}} = ctx.request;
    const file = fs.readFileSync(path);
    const params = {
        Bucket: "summer-images",
        Key: id,
        Body: file
    };
    s3.upload(params, function(err, data) {
        if (err) {
            throw err;
        }
    });
};