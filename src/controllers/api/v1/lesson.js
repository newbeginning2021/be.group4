const Lesson = require("../../../model/lesson");

exports.showCurrentDayClasses = async (ctx) => {
  const date = new Date().toJSON();
  const today = date.split('T')[0]
  const lessons = await Lesson.aggregate([
      {
          $match:{"date": today}
      },
      {
          $lookup:
          {
              from:"classes",
              localField:"classId",
              foreignField:"id",
              as:"classes",
          }
      },{
            $unwind: "$classes"
      },
      {
          $lookup:
          {
              from:"studentenrollments",
              localField:"classId",
              foreignField:"course_id",
              as:"studentenrollments"
          }
      },
      {
          $lookup:
          {
              from:"teachers",
              localField:"teacherId",
              foreignField:"id",
              as:"teachers",
              
          }
      },{
        $unwind: "$teachers"
      },
      {
          $project:{start_time:1, end_time:1, 
              "classes.name": 1, 
              "classes.id":1,
              "studentenrollments.student_id":1,
              "teachers.name": 1,
              "teachers.id":1,
              "teachers.img":1,
          }
      },
  ],(err,docs)=>{
      if(err){
          console.log(err);
          return;
      }
      console.log(docs);
  })

  ctx.body={
      lessons,
  }
}


exports.store = async (ctx) => {
  const requestBody = ctx.request.body;
  const latest = await Lesson.find().sort({ _id: -1 }).limit(1)
  if (latest.length !== 0) {
    const latestId = latest[0].id
    for (let index in requestBody) {
      const newData = {
        ...requestBody[index],
        id: `${latestId * 1 + index * 1 + 1}`
      }
      const newLesson = new Lesson(newData);
      const result = await newLesson.save()
      if (!result) {
        ctx.status = 500
      } else {
        ctx.status = 200;
      }
    }
  } else {
    for (let index in requestBody) {
      const newData = {
        ...requestBody[index],
        id: `${1 + index*1}`
      }
      const newLesson = new Lesson(newData);
      const result = await newLesson.save()
      if (!result) {
        ctx.status = 500
      } else {
        ctx.status = 200;
      }
    }
  }
}

exports.show = async (ctx) => {
  const { id } = ctx.params;
  const searchResult = await Lesson.find({ classId: id });

  ctx.body = searchResult;
}

exports.showLessons = async (ctx) => { 
  const { class_id } = ctx.params; 
  const test = await Lesson.aggregate([{
    $match: {
      "classId": class_id
    }
  }])
  const result = await Lesson.aggregate([
    {
      $match: {
        "classId":class_id
      }
    },
    {
      $lookup: {
        from: 'teachers',
        localField: 'teacherId',
        foreignField: "id",
        as:"teacherInfo"
      }
    },
    {
      $lookup: {
        from: 'classrooms',
        localField: 'classRoomId',
        foreignField: "id",
        as:"classRoomInfo"
      }
    },
    {
      $lookup: {
        from: 'studentenrollments',
        localField: 'classId',
        foreignField: "course_id",
        as:"studentInfo"
    }
    },
    {
      $unwind: "$teacherInfo"
    },
    {
      $unwind: "$classRoomInfo"
    },
    {
      $project: {
        "_id":1,
        "date": 1,
        "start_time": 1,
        "end_time": 1,
        "teacherInfo.name": 1,
        "teacherInfo.id": 1,
        "teacherInfo.img": 1,
        "classRoomInfo.id": 1,
        "classRoomInfo.name": 1,    
        "studentInfo.student_id": 1,
      }
    }
  ]);
  console.log(result)

  ctx.body = result;
}

exports.getAllLessons = async (ctx) => {
  const res = await Lesson.aggregate([
    {
      $lookup: {
        from: 'classes',
        localField: 'classId',
        foreignField: "id",
        as: 'classInfo'
      }
    },
    {
      $lookup: {
        from: 'classrooms',
        localField: 'classRoomId',
        foreignField: 'id',
        as: 'classroomInfo'
      }
    }, 
    {
      $lookup: {
        from: 'studentenrollments',
        localField: 'classId',
        foreignField: 'course_id',
        as: 'studentInfo'
      },
    }  
  ])
  ctx.body = res
}