const User = require("../../../model/user");
const {sign}=require('jsonwebtoken')
const {JWT_SECRET} = require('../../../constants') 

exports.update = async (ctx) => { 
  const userId = ctx.params.id
  const userInfo = ctx.request.body;
  try {
    const res = await User.updateOne({ id: userId }, userInfo)
    const newUserInfo = await User.find({id:userId})
    if (newUserInfo) { 
      ctx.body = newUserInfo
      ctx.status = 200
    }
  } catch (e) {
    ctx.body = {
      message: e.message,
      isUser:false
    }
  }
}


exports.store = async (ctx) => { 
  const userInfo = ctx.request.body;
  const currentId = await User.find()
  const currentSum = currentId.length;
  userInfo.id = `${currentSum + 1}`
  const newUser = new User(userInfo);
  try {
    const result = await newUser.save()
    if (result) { 
      const token = sign({ id: result.id }, JWT_SECRET, { expiresIn: '24h' })
      ctx.body = { 
        isUser: true,
        ...result,
        token
     };
    }
    ctx.status = 200
  } catch (e) {
    ctx.body = {
      message: e.message,
      isUser:false
    }
  }
}

exports.login = async (ctx) => {
    const emailOrUsername = ctx.request.body.emailOrUsername;
    console.log(ctx.request.body);
    const password = ctx.request.body.password;
    const user = await User.findByCredentials(emailOrUsername, password)
    if (user) {
        const token = sign({id:user.id}, JWT_SECRET, {expiresIn:'24h'} )
        const name = user.name;
        const email = user.email;
        const school_id = user.school_id;
        ctx.body = { 
          isUser: true,
          name,
          email,
          token,
          school_id
         };
         console.log(token)
    } else {
        ctx.body = { 
            isUser: false ,
            message: 'username or password is not valid!'
        };
    }
}
exports.checkAccount = async (ctx) => { 
  const { accountName } = ctx.params;
  const result = await User.find({ email: accountName });
  if (result.length === 0) {
    ctx.body = {
      valid:true
    };
  } else { 
    ctx.body = {
      valid:false
    };
  }
  ctx.status = 200
}


exports.show = async (ctx) => {
    const { keyword } = await ctx.request.params;
    console.log(keyword);
    const res = await User.find({
        $or: [{
            "email": keyword
        },
        {
            "name": keyword
        }
    ]
    });
    if(!res) {
        ctx.body = {
            message: "not found",
        }
    } else {
        ctx.body = res
    }
}
