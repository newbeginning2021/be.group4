const Classroom = require("../../../model/classRoom");

exports.index = async (ctx) => {
  const myUrl = new URL(ctx.url, "http:localhost:3000") 
  const school_id = myUrl.searchParams.get('school_id')
    try{
    const res = await Classroom.find({school_id});
    ctx.body = res;
    }catch(e){
        ctx.body = e;
    }
};

exports.getCLassrooms = async (ctx) => {
  const result = await Classroom.find()
  if (result) { 
    ctx.body = result
    ctx.status = 200
  } else (
    ctx.status = 400
  )
}

exports.store = async (ctx) => {
    const {body} = ctx.request;
    const classroom = new Classroom(body);
    try {
        await classroom.save();
        ctx.status = 200;
    } catch (e) {
        ctx.body = e;
    }
};

exports.delete = async (ctx) => {
    const { id } = ctx.params;
    const { n } = await Classroom.deleteOne({ _id: id });
    if (n === 0) {
        ctx.body = {
        message: `${id} not found!`,
        };
        ctx.status = 404;
    } else {
        ctx.body = {
        message: `${id} deleted!`,
        };
    }
};
  
exports.show = async (ctx) => {
    const { id } = ctx.params;
    const res = await Classroom.find({ _id: id });
    ctx.body = res;
};