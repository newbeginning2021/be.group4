const StudentEnrollment = require('../../../model/studentClassEnrollment');
const Student = require("../../../model/student");



exports.delete = async (ctx) => {
  const { id } = ctx.params;
    try{
      const {n} = await StudentEnrollment.deleteOne({id});
      if(n===0){
        ctx.body = {
          message: `${id} cannot be found!`
        }
      }else {
          ctx.status = 201
          ctx.body = {
            message: 'deleted!'
          }
      }
    }catch(e){
      ctx.body = e;
    }
}

exports.store = async (ctx) => {
  const requestBody = ctx.request.body;
  const latest = await StudentEnrollment.find().sort({ _id: -1 }).limit(1)
  if (latest.length !== 0) {
    const latestId = latest[0].id
    for (let index in requestBody) {
      const newData = {
        ...requestBody[index],
        id: `${latestId * 1 + index * 1 + 1}`
      }
      const newEnrols = new StudentEnrollment(newData);
      const result = await newEnrols.save()
      if (!result) {
        ctx.status = 500
      } else {
        ctx.status = 200;
      }
    }
  } else {
    for (let index in requestBody) {
      const newData = {
        ...requestBody[index],
        id: `${1 + index*1}`
      }
      const newEnrols = new StudentEnrollment(newData);
      const result = await newEnrols.save()
      if (!result) {
        ctx.status = 500
      } else {
        ctx.status = 200;
      }
    }
  }
}

exports.getEnrols = async (ctx) => { 
  const { id } = ctx.params;
  let check = await Student.aggregate([
      {
          $match: {'id': id}
      },
      {
          $project: {'account':0,'password':0}
      }
  ])
  let check2 = await StudentEnrollment.aggregate([
      {
          $match: { 'student_id': id }
      },
      {
          $lookup: {
              from: 'classes',
              localField: 'course_id',
              foreignField: 'id',
              as: 'class_details'
          }
      },
      {
          $project: { 'student_id': 1, 'enrol_id': '$id', 'class_details.name': 1, 'class_details.id': 1, '_id': 0 }
      },
      {
          $unwind: '$class_details'
      },
      {
          $group: {
              _id: '$student_id',
              enrol_details: { $push: { enrol_id: "$enrol_id", class_details: "$class_details" } }
          }
      },
      {
          $project: {'_id':0}
      },
  ])
  check.push(check2[0])
  ctx.body = check2;
}

exports.enrolDetail = async (ctx) => {
  let result = {}
  if (ctx.enrols.length !== 0) {
    result = ctx.enrols[0];
  } else { 
    result = {
      "course_detail":[],
      "student_id": ctx.params.id,
    }
  }
  result.unEnrols = ctx.unEnrols
  ctx.body = result;
}



