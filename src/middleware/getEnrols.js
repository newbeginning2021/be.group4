const StudentEnrol =require('../model/studentClassEnrollment');
const Student = require('../model/student');
  
const getEnrols = async (ctx, next) => {
  let { id } = ctx.params;
  const studentInfo = await Student.findOne({ id })
  const {school_id} = studentInfo
  let enrols = await StudentEnrol.aggregate([
      {
          $match: { student_id: id }
      },
      {
          $lookup: {
              from: 'classes',
              localField: 'course_id',
              foreignField: 'id',
              as: 'course_details'
          }
      },
      {
          $project: { 'course_details.name': 1, 'course_details.id': 1, 'student_id': 1, _id: 0 }
      },
      {
          $unwind: '$course_details'
      },
      {
          $group: {
              _id: '$student_id',
              course_detail: {
                  $push: { course_id: '$course_details.id', course_name: '$course_details.name' }
              },
          }
      },
      {
          $project: { student_id: '$_id' , '_id':0,  course_detail: 1}
      }
  ])
  ctx.enrols = enrols
  ctx.schoolId = school_id
  await next();
}

module.exports = getEnrols