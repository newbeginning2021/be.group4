const {JWT_SECRET} = require('../constants') 
const jwt = require('jsonwebtoken')

exports.authenticateToken = async (ctx, next) => {
  const authHeader = ctx.request.header.authorization;
    if(authHeader) {
        const token = authHeader.split(' ')[1];
        try {
            await jwt.verify(token, JWT_SECRET);
            return next();
          } catch(err) {
            ctx.throw(401, {
              data: {
                error: 'UNAUTHORIZED',
                message: 'Invalid token.'
              }
            });
          }
    } else {
        ctx.throw(401, {
            data: {
              error: 'UNAUTHORIZED',
              message: 'Invalid token.'
            }
          });
    }
}
