const Lesson = require("../model/lesson");
const Course = require("../model/class");

const oneDayLoad = 1000 * 3600 * 24;

const weekdaysArray = [
  "sunday",
  "monday",
  "thuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
];

const getDayIndex = (dateString) => {
  const date = new Date(dateString);
  const dayIndex = date.getDay();
  return dayIndex;
};

function classScheduleMaker(startDate, endDate, dayIndex) {
  const startDateIndex = getDayIndex(startDate);
  const endDateStamp = Date.parse(new Date(endDate));

  const dayGap = dayIndex - startDateIndex;

  let startTimeStamp = Date.parse(new Date(startDate)) + dayGap * oneDayLoad;
  let classScheduleTimestamps = [];

  while (startTimeStamp <= endDateStamp) {
    classScheduleTimestamps = [...classScheduleTimestamps, startTimeStamp];
    startTimeStamp += 7 * oneDayLoad;
  }

  dayGap > 0 ? classScheduleTimestamps : classScheduleTimestamps.shift();
  const classSchedule = classScheduleTimestamps.map((el) => {
    return new Date(el);
  });
  return classSchedule;
}

exports.getClassSchedule = async (ctx) => {
  const {
    id,
    start_date,
    end_date,
    start_time,
    end_time,
    day,
  } = ctx.request.body;

  day.forEach((el) => {
    const scheduleList = classScheduleMaker(start_date, end_date, el);
    scheduleList.forEach(async (el) => {
      const lesson = new Lesson({
        date: el,
        start_time: start_time,
        end_time: end_time,
        classId: id,
      });
      await lesson.save();
    });
  });
};
