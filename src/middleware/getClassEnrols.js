const StudentEnrol =require('../model/studentClassEnrollment')
const Class= require('../model/class')
const getClassEnrols = async (ctx, next) => {
  let { id } = ctx.params;
  let classenrols = await StudentEnrol.aggregate([
      {
          $match: { course_id : id }
      },
      {
          $lookup: {
              from: 'students', 
              localField: 'student_id',
              foreignField: 'id',
              as: 'students_details'
          }
      },
      {
          $project: { 'students_details.name': 1, 'students_details.id': 1, 'course_id': 1, _id: 0 }
      },
      {
          $unwind: '$students_details'
      },
      {
          $group: {
              _id: '$course_id',
              students_detail: {
                  $push: { student_id: '$students_details.id', student_name: '$students_details.name' }
              },
          }
      },
      {
          $project: { course_id: '$_id' , '_id':0,  students_detail: 1}
      }
  ])
  ctx.classenrols = classenrols
  await next();
}

module.exports = getClassEnrols