const objectIdValidation = require('./objectIdValidation')
const getEnrols = require('./getEnrols')
const getUnEnrols = require('./getUnEnrols')
const {getClassSchedule} = require('./getClassSchedule')
const getClassEnrols=require('./getClassEnrols')
const getClassUnEnrols=require('./getClassUnenrols')
module.exports = {
  getEnrols,
  getUnEnrols,
  getClassSchedule,
  objectIdValidation,
  getClassEnrols,
  getClassUnEnrols
};
