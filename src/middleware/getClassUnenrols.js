const Class =require('../model/class');
const Student = require('../model/student');
  
const getClassUnEnrols = async (ctx, next) => {
  let classenrolId = []
  if (ctx.classenrols.length !== 0) { 
    const { students_detail } = ctx.classenrols[0];
    students_detail.forEach((item) => { 
      classenrolId.push(item.student_id)
    })
  }
  const classunEnrols = await Student.aggregate([
    {
      $match: {
        id: { $nin: classenrolId },
      }
    },
    {
      $project: {
        name: 1,
        id: 1,
        _id: 0,
      }
    }
  ])
  ctx.classunEnrols = classunEnrols
  await next();
}

module.exports = getClassUnEnrols