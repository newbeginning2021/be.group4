const Class =require('../model/class')
  
const getUnEnrols = async (ctx, next) => {
  let enrolId = []
  if (ctx.enrols.length !== 0) { 
    let { course_detail } = ctx.enrols[0];
    course_detail.forEach((item,index) => { 
      enrolId.push(item.course_id)
    })
  }
  let unEnrols = await Class.aggregate([
    {
      $match: {
        id: { $nin: enrolId },
        school_id: ctx.schoolId
      }
    },
    {
      $project: {
        name: 1,
        id: 1,
        _id: 0,
      }
    }
  ])
  console.log('this is unEnrols')
  console.log(unEnrols)
  ctx.unEnrols = unEnrols
  await next();
}

module.exports = getUnEnrols