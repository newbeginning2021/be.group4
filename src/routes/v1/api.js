const userController = require('../../controllers/api/v1/user');
const studentController = require('../../controllers/api/v1/student');
const studentEnrollmentController = require('../../controllers/api/v1/studentEnrollment');
const teacherController = require('../../controllers/api/v1/teacher');
const teacherAllocationController = require('../../controllers/api/v1/teacherAllocation');
const classController = require('../../controllers/api/v1/class');
const classroomController = require('../../controllers/api/v1/classroom')
const schoolController = require('../../controllers/api/v1/school');
const lessonController = require('../../controllers/api/v1/lesson');
const middleController = require('../../middleware')
const {authenticateToken} = require('../../middleware/authenticateToken')

const koaRouter = require("koa-router");

const router = new koaRouter();

router.post("/login", userController.login);
router.get('/user/checkAccount/:accountName', userController.checkAccount)
router.post("/users", userController.store);
router.put("/users/:id", authenticateToken, userController.update);

router.get("/user/:keyword", userController.show);


router.get("/students", authenticateToken, studentController.index);
router.post("/students", authenticateToken, studentController.store);
router.delete("/students/:id", authenticateToken, studentController.delete);
router.get("/students/:id", authenticateToken, studentController.show);
router.put("/students/:id", authenticateToken, studentController.update);

router.get("/studentList", authenticateToken, studentController.studentList);

router.delete("/batchstudent", authenticateToken, studentController.batchDelete);

router.get("/students/searchByKeyword/:keyword", authenticateToken, studentController.findByAttribute);


router.get("/teachers", authenticateToken, teacherController.index);
router.post("/teachers", authenticateToken, teacherController.store);
router.delete("/teachers/:id", authenticateToken, teacherController.delete);
router.get("/teachers/:id", authenticateToken, teacherController.show);
router.put("/teachers/:id", authenticateToken, teacherController.update);
router.get("/teacherList", authenticateToken, teacherController.teacherList);
router.delete("/batchTeacher", authenticateToken, teacherController.batchDelete);
router.get("/teachers/searchByKeyword/:keyword", authenticateToken, teacherController.findByAttribute);
router.get("/teacherallocation/:id",authenticateToken, teacherAllocationController.getOneTeacherClass);
router.post("/teacherallocation", authenticateToken,teacherAllocationController.addTeacherClass);
router.post("/postData", authenticateToken, teacherController.postData);
router.get("/allteachers", authenticateToken, teacherController.getTeachers);

router.get("/classes", authenticateToken, classController.index);
router.post("/classes", authenticateToken, classController.store);
router.post(
  "/classes",
  authenticateToken,
  classController.store,
  middleController.getClassSchedule
);

router.get("/classList", authenticateToken, classController.classList);
router.delete("/classes/:id", authenticateToken, classController.delete);
router.get("/classes/:id", authenticateToken, classController.show);
router.get("/classes/lessons/:id", authenticateToken, lessonController.show);
router.post("/classes/lessons/:id", authenticateToken, lessonController.store);
router.put("/classes/:id", authenticateToken, classController.update);
router.get("/filter/class/:id", authenticateToken, classController.getStudents);
router.get("/classesDetail/:classId", authenticateToken, classController.getClassDetail);
router.get(
  "/classes/:id/unenrolDetail",
  authenticateToken,
  middleController.getClassEnrols,
  middleController.getClassUnEnrols,
  classController.unenrolDetail
);
router.post("/classes/enrolStudent", authenticateToken, classController.enrolStudent);
router.get("/class/teacher", authenticateToken, classController.getTeachers);
router.get("/class/classroom", authenticateToken, classController.getClassrooms);
router.get("/class/teacher/:id", authenticateToken, classController.getClassesByTeacher);
router.get("/class/classroom/:id", authenticateToken, classController.getClassesByClassroom);
router.get("/class/keyword/:keyword", authenticateToken, classController.getClassesByKeyword);
router.get("/classesInfo", authenticateToken, classController.getClassList);
router.get("/classSelect", authenticateToken, classController.classSelect);

router.get("/dashboard", authenticateToken, lessonController.showCurrentDayClasses);
router.get("/lessons/:class_id", authenticateToken, lessonController.showLessons)
router.post("/lessons", authenticateToken, lessonController.store)
router.get("/classes/lessons/:id", authenticateToken, lessonController.show);
router.post("/classes/lessons/:id", authenticateToken, lessonController.store);
router.get("/lessons", lessonController.getAllLessons);


router.get("/schools", authenticateToken, schoolController.index);
router.post("/schools", authenticateToken, schoolController.store);
router.delete("/schools/:id", authenticateToken, schoolController.delete);
router.get("/schools/:id", authenticateToken, schoolController.show);
router.put("/schools/:id", authenticateToken, schoolController.update);
router.get("/showRelatedSchools/:name", authenticateToken, schoolController.showRelatedSchools);


router.post("/studentEnrollment", authenticateToken, studentEnrollmentController.store);
router.delete("/studentEnrollment/:id", authenticateToken, studentEnrollmentController.delete);
router.get("/studentEnrollment/enrols/:id", authenticateToken, studentEnrollmentController.getEnrols);

router.get(
  "/studentEnrollment/:id/enrolDetail",
  authenticateToken,
  middleController.getEnrols,
  middleController.getUnEnrols,
  studentEnrollmentController.enrolDetail
);

router.get("/classroom", authenticateToken, classroomController.index);
router.post('/classroom', authenticateToken, classroomController.store);
router.get('/classroom/:id', authenticateToken, classroomController.show)
router.get("/classrooms", authenticateToken, classroomController.getCLassrooms);
module.exports = router;
