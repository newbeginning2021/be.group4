const koa = require('koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const formidable = require('koa2-formidable');
const loader = require('./src/loaders');

const app = new koa();
app.use(cors());
app.use(formidable());
app.use(bodyParser());
loader.init(app);

module.exports = app;